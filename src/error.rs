use thiserror::Error;
use winit::error::OsError;

#[derive(Error, Debug)]
pub enum Error {
    #[error("unable to create window for emulator")]
    WindowError(#[from] OsError),

    #[error("unable to initialise the pixels in the window")]
    PixelsError(#[from] pixels::Error),
}

pub type Result<T> = std::result::Result<T, Error>;
