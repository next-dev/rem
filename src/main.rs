#![allow(dead_code, unused)]

mod error;
mod spectrum48;

use std::{
    thread,
    time::{Duration, Instant},
};

use pixels::{Pixels, SurfaceTexture};
use pixels_u32::PixelsExt;
use spectrum48::Spectrum48;
use winit::{
    dpi::PhysicalSize,
    event::{ElementState, Event, KeyboardInput, VirtualKeyCode, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};

use crate::error::Result;

const SCREEN_WIDTH: u32 = 320;
const SCREEN_HEIGHT: u32 = 256;
const SCREEN_SCALE: u32 = 4;

const WINDOW_WIDTH: u32 = SCREEN_WIDTH * SCREEN_SCALE;
const WINDOW_HEIGHT: u32 = SCREEN_HEIGHT * SCREEN_SCALE;

fn main() -> Result<()> {
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_inner_size(PhysicalSize::new(WINDOW_WIDTH, WINDOW_HEIGHT))
        .with_title("REM")
        .with_resizable(false)
        .build(&event_loop)?;

    let mut pixels = {
        let surface_texture = SurfaceTexture::new(WINDOW_WIDTH, WINDOW_HEIGHT, &window);
        Pixels::new(SCREEN_WIDTH, SCREEN_HEIGHT, surface_texture)
    }?;

    let mut machine = Spectrum48::new();
    // let screen = include_bytes!("../etc/AticAtac.scr");
    let screen = include_bytes!("../etc/Jetpac.scr");
    machine.get_memory_mut().dump(0x4000, screen);

    let mut time = Instant::now();

    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Poll;

        match event {
            Event::WindowEvent { window_id, event } if window.id() == window_id => match event {
                WindowEvent::CloseRequested
                | WindowEvent::KeyboardInput {
                    input:
                        KeyboardInput {
                            state: ElementState::Pressed,
                            virtual_keycode: Some(VirtualKeyCode::Escape),
                            ..
                        },
                    ..
                } => *control_flow = ControlFlow::Exit,
                _ => {}
            },
            Event::MainEventsCleared => {
                window.request_redraw();
            }
            Event::RedrawRequested(_) => {
                if Instant::now() >= time {
                    redraw(&mut pixels, &mut machine);
                    time += Duration::from_millis(20);
                    *control_flow = ControlFlow::WaitUntil(time);
                }
            }
            _ => {}
        }
    });
}

fn redraw(pixels: &mut Pixels, machine: &mut Spectrum48) {
    // U32 is 0xAABBGGRR
    let frame = pixels.get_frame_u32();

    for pixel in frame.iter_mut() {
        *pixel = 0xffff00ff;
    }

    machine.update_video(frame);
    machine.next_frame();

    pixels.render().unwrap();
}
