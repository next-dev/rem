use std::{mem::swap, num::Wrapping};

use pixels::Pixels;

use crate::{SCREEN_HEIGHT, SCREEN_WIDTH};

const DISPLAY_WIDTH: u32 = 256;
const DISPLAY_HEIGHT: u32 = 192;

const TOP_BORDER_WIDTH: u32 = (SCREEN_HEIGHT - DISPLAY_HEIGHT) / 2;
const BOTTOM_BORDER_WIDTH: u32 = TOP_BORDER_WIDTH;
const LEFT_BORDER_WIDTH: u32 = (SCREEN_WIDTH - DISPLAY_WIDTH) / 2;
const RIGHT_BORDER_WIDTH: u32 = LEFT_BORDER_WIDTH;

/// The Spectrum 48K machine
pub struct Spectrum48 {
    memory: Spectrum48Memory,
    border_colour: u8,
    frame_counter: Wrapping<u8>,
}

/// Represents the RAM and ROM of the ZX Spectrum 48
///
/// # Memory Map of the ZX Spectrum 48K
///
/// 0000 - ROM (16K) contains BASIC
/// 4000 - Pixel display (256x192 pixels)
/// 5800 - Attribute display (32x24 attributes)
/// 5B00 - RAM
/// FFFF - End of RAM
///
pub struct Spectrum48Memory {
    bytes: [u8; 65536],
}

impl Spectrum48Memory {
    fn peek(&self, address: u16) -> u8 {
        self.bytes[address as usize]
    }

    fn poke(&mut self, address: u16, byte: u8) {
        self.bytes[address as usize] = byte;
    }

    pub fn dump(&mut self, address: u16, bytes: &[u8]) {
        let mut addr = address;

        for byte in bytes {
            self.poke(addr, *byte);
            addr += 1;
        }
    }
}

impl Spectrum48 {
    pub fn new() -> Self {
        Self {
            memory: Spectrum48Memory { bytes: [0; 65536] },
            border_colour: 7,
            frame_counter: Wrapping(0),
        }
    }

    pub fn get_memory(&self) -> &Spectrum48Memory {
        &self.memory
    }

    pub fn get_memory_mut(&mut self) -> &mut Spectrum48Memory {
        &mut self.memory
    }

    #[allow(clippy::unusual_byte_groupings)]
    pub fn update_video(&self, pixels: &mut [u32]) {
        // For each byte in the attribute file, it sets the ink & paper colours
        // for the 8x8 square it's associated with.  The format of the byte is:
        //
        //  76543210
        //  --------
        //  FBPPPIII
        //
        //  F = Flash
        //  B = Bright
        //  P = Paper (or background colour)
        //  I = Ink (or foreground colour)
        //
        // The pixel memory address is calculated thus:
        //
        //       1          0
        //  54321098 76543210
        //  -----------------
        //  010SSLLL RRRCCCCC
        //
        //  C = column number (0-31)
        //  R = character row number (0-7)
        //  L = lines within a character (0-7)
        //  S = screen # (third of display) (0-2)
        //
        //       1          0
        //  54321098 76543210
        //  -----------------
        //  010110RR RRRCCCCC
        //
        //  C = column number (0-31)
        //  R = row number (0-23)

        const COLOURS: [u32; 16] = [
            //AABBGGRR
            0xff000000, // 0 - Black
            0xffd70000, // 1 - Blue
            0xff0000d7, // 2 - Red
            0xffd700d7, // 3 - Magenta
            0xff00d700, // 4 - Green
            0xffd7d700, // 5 - Cyan
            0xff00d7d7, // 6 - Yellow
            0xffd7d7d7, // 7 - White
            0xff000000, // 8 - Black (again)
            0xffff0000, // 9 - Bright blue
            0xff0000ff, // A - Bright red
            0xffff00ff, // B - Bright magenta
            0xff00ff00, // C - Bright green
            0xffffff00, // D - Bright cyan
            0xff00ffff, // E - Bright yellow
            0xffffffff, // F - Bright white
        ];

        let mut i = 0;
        let border_colour = COLOURS[self.border_colour as usize];

        // Render the top border
        for row in 0..TOP_BORDER_WIDTH {
            for col in 0..SCREEN_WIDTH {
                pixels[i] = border_colour;
                i += 1;
            }
        }

        //  010SSLLL RRRCCCCC   (Pixels)
        //  010110RR RRRCCCCC   (Attrs)
        let mut a_addr = 0x5800u16;
        for third in 0u16..3 {
            let mut p_addr = 0x4000u16 + (third * 2048);
            for row in 0..8 {
                for line in 0..8 {
                    let mut a_addr: u16 = 0x5800 + (((third * 8) + row) << 5);
                    let mut p_addr: u16 = 0x4000 + (third << 11) + (line << 8) + (row << 5);

                    // Draw left border
                    for col in 0..LEFT_BORDER_WIDTH {
                        pixels[i] = border_colour;
                        i += 1;
                    }

                    //  Draw screen line
                    for col in 0..32 {
                        let attr = self.memory.peek(a_addr);
                        let mut p = self.memory.peek(p_addr);
                        a_addr += 1;
                        p_addr += 1;

                        let bright = if (attr & 0b01_000_000) != 0 { 8u8 } else { 0 };
                        let mut ink = (attr & 0b00_000_111) + bright;
                        let mut paper = ((attr & 0b00_111_000) >> 3) + bright;
                        if ((attr & 0b10_000_000) != 0) && ((self.frame_counter.0 & 16) != 0) {
                            swap(&mut ink, &mut paper);
                        }

                        for x in 0..8 {
                            let pixel = p & 0x80;
                            let colour = if pixel == 0x80 { ink } else { paper };
                            p = (p & 0x7f) << 1;

                            pixels[i] = COLOURS[colour as usize];
                            i += 1;
                        }
                    }

                    // Draw right border
                    for col in (LEFT_BORDER_WIDTH + DISPLAY_WIDTH)..SCREEN_WIDTH {
                        pixels[i] = border_colour;
                        i += 1;
                    }
                }
            }
        }

        // Render the top border
        for row in 0..BOTTOM_BORDER_WIDTH {
            for col in 0..SCREEN_WIDTH {
                pixels[i] = border_colour;
                i += 1;
            }
        }
    }

    pub fn next_frame(&mut self) {
        self.frame_counter += 1;
        if self.frame_counter.0 % 64 == 0 {
            println!("Frame {}", self.frame_counter);
        }
    }
}
