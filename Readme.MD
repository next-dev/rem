# Rust-based Emulator Program

This is an emulator system written in Rust.  I intend to emulate many machines
under one program.  Initially, I will emulate the 48K ZX Spectrum.

